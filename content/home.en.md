+++

date = "2016-12-05T14:41:00+01:00"
title = "Home"
author = "Framasoft"
draft = false
type = "page"
id = "-home"
[menu]
     [menu.main]
        name = "Home"
        weight = 1
        identifier = "home"
+++

# Contributopia
## To de-google-ify is not enough

<a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> invites you to join us on a shared adventure: to explore the digital worlds where humanity and our fundamental freedoms are respected, respected right through to the creation of the tools that we use.

During this journey, planned for 2017 to 2020, let us set out to define the digital tools which make possible our contributions in all fields of our activity and in all types of creativity.

[more info](https://framablog.org/2017/10/09/contributopia-degoogliser-ne-suffit-pas/)

</p>
{{% grid class="row" %}}
{{% grid class="col-sm-4 text-center" %}}

[![](/menu-services.png)](../services)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### 2017-2018: Create and offer tools

At <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a>, thanks to three years of the <a href="https://degooglisons-internet.org"><b class="violet">De-google-ify</b>&nbsp;<b class="orange">Internet</b></a> campaign, which has put in place more than thirty alternative services, we know that the web giants don't just siphon off our digital lives: by their very design, they shape our habits, our actions, even the way we think.

In the exploration of this first world, you'll find online services that are designed with a different set of values, respecting the (private) lives of users and the diversity that makes up our networks. In this world are conceived the digital tools that allow us to share our ideas, our opinions, our meetings&hellip; even our videos!

[more info](../services)

{{% /grid %}}
{{% /grid %}}
{{% grid class="row" %}}
{{% grid class="col-sm-4 text-center" %}}

[![](/menu-essaimage.png)](../essaimage)

{{% /grid %}}
{{% grid class="col-sm-8" %}}
    
### 2018-2019: Pass on know-how

The web giants have based their model on centralising our data, seducing users with services which actually lock our digital existence into gigantic digital silos filled with our data: records of our clicks, our conversations, our personal life. To reproduce this centralised model is to concentrate power and the dangers that come with it.

Let us discover a world in which we don't put all our lives in one single digital basket! A world in which, faced with the digital supermarkets that deal in our data, we organise digital "cooperatives" that are local and based on exchange. A world in which <abbr title="Keen Internet Talented Teams Engaged in Network Services">KITTENS</abbr> abound, a world with personal clouds, where we teach each other, with international cross-pollination.

[more info](../essaimage)

{{% /grid %}}
{{% /grid %}}
{{% grid class="row" %}}
{{% grid class="col-sm-4 text-center" %}}

[![](/menu-educ-pop.png)](../educ-pop)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### 2019-2020: Inspire the possibilities

There is a vision behind the services that the web giants offer. It reduces our intimacy to a commodity for the benefit of advertisers and marketers. This diminishes our role to that of a consumer and we are abandoning our freedom for more comfort. There is no room in this schema for digital tools that enable possibilities like participation, contribution and popular education.

That leaves an as yet unexplored world for us to discover and cultivate together, equipped with tools designed to develop new ways to team up and to create commons. A world that is inspired by countless communities (such as the free software community), to help each of us to choose the tools that work for us, to become digitally autonomous, to collaborate differently and to share knowledge.

[more info](../educ-pop)

{{% /grid %}}
{{% /grid %}}
<p>

+++

date = "2016-12-05T14:41:00+01:00"
title = "Popular education"
author = "Framasoft"
draft = false
type = "page"
id = "-educ-pop"
[menu]
     [menu.main]
        name = "Popular education"
        weight = 1
        identifier = "educ-pop"
+++

</p>
{{% grid class="row" %}}
{{% grid class="col-sm-8 col-sm-offset-4" %}}

# Popular education
## Inspire the possibilities

Welcome to a world where everyone can share, and access, knowledge!

In 2019 and 2020 and after the important work of meetings, exchanges and collaborations, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> proposes to concentrate on digital tools that facilitate access to shared knowledge.

It's a world that is as yet uncharted, because it needs to be imagined together, with the actors who, each day, define a society where we contribute more than we consume, the society of contribution.

{{% /grid %}}
{{% /grid %}}
<!-- mediation -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/educ-pop-outils.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Access for all

As things stand, finding the libre web service which corresponds to your needs requires a great deal of know-how and remains difficult to access to those of us who are not at ease using digital tools.

It is by working jointly with these people, experts in digital accessibility, and professionals in design and user experience that <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> desires to tackle the question, while at the same time offering tools and training to the actors from territorial networks who work on digital access.

The aim? Creating the right tools and conditions to enable access to an ethical web for all.


{{% /grid %}}
{{% /grid %}}
<!-- git -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/educ-pop-git-moldus.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}
    

### Git within reach of all of us

_Git_ is a tool which allows people to collaborate collectively on code. The way that it has been designed opens a magnificent gateway to understanding the methods, practices and frame of mind involved in collaboration.

How can we make it so that people who don't code can benefit from the philosophy of such a tool? How can we adapt _Git_, its jargon, its interface, etc. so that everyone can co-create and collaborate on texts or other contributed commons?

There are projects that exist already and it is with them that <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> will study possible solutions.

{{% /grid %}}
{{% /grid %}}
<!-- mooc chatons -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/educ-pop-mooc-chatons.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}


### A MOOC for the <abbr title="Collective of Keen Internet Talented Teams Engaged in Network Services">KITTENS</abbr>

Becoming a <abbr title="Collective of Keen Internet Talented Teams Engaged in Network Services">KITTEN</abbr>, a member of this collective of organisations which host ethical web services, is not an easy task: there is an impressive collection of know-how to acquire and of practices to understand.

It is with the members of this collective that <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> is proposing to create a MOOC, a massive open online course, to bring together and layout the know-how we want to share&hellip; and may the <abbr title="Collective of Keen Internet Talented Teams Engaged in Network Services">KITTENS</abbr> in our collective basket multiply!

{{% /grid %}}
{{% /grid %}}
<!-- upload -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/educ-pop-upload.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### UPLOAD, University of the People of Libre

It is the project that is the least clear and perhaps the most crazy of this Contributopia: creating a University of the People of Libre that is Open, Accessible and Decentralised.

The desire is to create a digital tool where each and every one of us can share our know-how and find knowledge, or even (and let us dream) have it established.

It is difficult to describe today how this University of the People of Libre will be founded, since its construction will depend on the good will, contributions and partnerships that can be seized over the course of this voyage.

{{% /grid %}}
{{% /grid %}}
<p>
